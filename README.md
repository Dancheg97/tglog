# TG notify

Simple go library to add logrus logging hook to send `error`/`fatal`/`panic`/`warn` messages to telegram.

Can be connected to your service by providing:

- Dev telegram bot token (token of dev bot, don't pass regular ones)
- Connection password (will be asked on bot connection)

```go
package main

import (
	"time"

	"github.com/sirupsen/logrus"
	"codeberg.org/dancheg97/tglog"
)

func main() {
	_, err := tglog.AddTelegramLogger("telegram:token", "connect_password")
	if err != nil {
		panic(err)
	}

	for {
		time.Sleep(time.Second * 10)
		logrus.Error("error message sent")
		time.Sleep(time.Second)
		logrus.Warn("warn message sent")

		logrus.WithFields(logrus.Fields{
			"uno":  "due",
			"hehe": "hehe1",
		}).Error("test")
	}
}

```

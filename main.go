package tglog

import (
	"bytes"
	"errors"
	"fmt"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
)

// This function will add logging errors to telegram channel,
// returning modified version of logrus logger.
func AddTelegramLogger(token, password string) (*logrus.Logger, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return nil, err
	}

	hook := TgHook{Bot: bot, Receivers: map[int64]struct{}{}}

	locker := map[int64]int{}

	go func() {
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates := bot.GetUpdatesChan(u)

		for update := range updates {
			if update.Message != nil {
				if update.Message.Text != password {
					locker[update.Message.Chat.ID] += 1

					if locker[update.Message.Chat.ID] == 15 {
						bot.Send(tgbotapi.NewMessage(
							update.Message.Chat.ID,
							"blocked, too many incorrect passwords",
						))
						continue
					}

					bot.Send(tgbotapi.NewMessage(
						update.Message.Chat.ID,
						"password incorrect, try again",
					))
				} else {
					bot.Send(tgbotapi.NewMessage(
						update.Message.Chat.ID,
						"notify chat connected",
					))

					hook.Lock()
					hook.Receivers[update.Message.Chat.ID] = struct{}{}
					hook.Unlock()
				}
			}
		}
	}()

	logger := logrus.StandardLogger()
	logger.AddHook(&hook)

	return logger, nil
}

type TgHook struct {
	Bot       *tgbotapi.BotAPI
	Receivers map[int64]struct{}
	sync.Mutex
}

func (h *TgHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.ErrorLevel,
		logrus.FatalLevel,
		logrus.PanicLevel,
		logrus.WarnLevel,
	}
}

func (h *TgHook) Fire(e *logrus.Entry) error {
	var errs []error

	for id := range h.Receivers {
		var buf bytes.Buffer
		for k, v := range e.Data {
			fmt.Fprintf(&buf, "[%s/%s] ", k, v)
		}
		buf.Write([]byte(e.Message))
		_, err := h.Bot.Send(tgbotapi.NewMessage(id, buf.String()))
		errs = append(errs, err)
	}

	return errors.Join(errs...)
}
